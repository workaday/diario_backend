<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateWorkadayRequest extends FormRequest
{
    public function authorize(): bool
    {
        return Auth::check();
    }

    public function rules(): array
    {
        return [
            'calls' => 'sometimes|nullable|string',
            'done' => 'sometimes|nullable|string',
            'contacts' => 'sometimes|nullable|array',
            'user_id' => 'sometimes|nullable|int|exists:users,id',
            'user_name' => 'sometimes|nullable|string',
        ];
    }
}
