<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateWorkadayRequest extends FormRequest
{
    public function authorize(): bool
    {
        return Auth::check();
    }

    public function rules(): array
    {
        return [
            'calls' => 'nullable|string',
            'done' => 'nullable|string',
            'contacts' => 'nullable|array',
            'user_id' => 'nullable|int|exists:users,id',
            'user_name' => 'nullable|string',
        ];
    }
}
