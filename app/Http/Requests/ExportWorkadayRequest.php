<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExportWorkadayRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => 'sometimes|nullable|exists:users,id',
            'start_date' => 'sometimes|nullable|date_format:Y-m-d',
            'end_date' => 'sometimes|nullable|date_format:Y-m-d|after_or_equal:start_date',
        ];
    }
}
