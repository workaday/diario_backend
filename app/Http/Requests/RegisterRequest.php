<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'job_description' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ];
    }
}
