<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\User\Contract\UserContract;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function __construct(protected readonly UserContract $userContract)
    {
    }

    public function get(): JsonResponse
    {
        return response()->json($this->userContract->getUsersByCompany());
    }

    public function getAllUserNames(): JsonResponse
    {
        return response()->json($this->userContract->getUsersName());
    }
}
