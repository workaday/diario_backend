<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Services\Auth\Contract\RegisterContract;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    public function __construct(protected readonly RegisterContract $registerContract)
    {
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        return response()->json($this->registerContract->makeRegister($request->validated()), 201);
    }
}
