<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\Auth\Contract\LoginContract;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    public function __construct(protected readonly LoginContract $loginContract)
    {
    }

    public function login(LoginRequest $request): JsonResponse
    {
        return response()->json($this->loginContract->makeLogin($request->validated()['email']));
    }
}
