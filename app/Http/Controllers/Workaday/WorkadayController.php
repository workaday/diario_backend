<?php

namespace App\Http\Controllers\Workaday;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateWorkadayRequest;
use App\Http\Requests\UpdateWorkadayRequest;
use App\Models\Workaday;
use App\Services\Workaday\Contract\WorkadayContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class WorkadayController extends Controller
{
    public function __construct(protected readonly WorkadayContract $workadayContract)
    {
    }

    public function post(CreateWorkadayRequest $request): JsonResponse
    {
        return response()->json($this->workadayContract->create($request->validated()), 201);
    }

    public function get(): JsonResponse
    {
        return response()->json($this->workadayContract->getAllWorkaday());
    }

    public function delete(Workaday $workaday): Response
    {
        $workaday->delete();

        return response()->noContent();
    }

    public function put(UpdateWorkadayRequest $request, Workaday $workaday): JsonResponse
    {
        return response()->json($this->workadayContract->update($workaday, $request->validated()));
    }

    public function show(Workaday $workaday): JsonResponse
    {
        return response()->json($workaday);
    }
}
