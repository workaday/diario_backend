<?php

namespace App\Http\Controllers\Workaday;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExportWorkadayRequest;
use App\Services\Workaday\Contract\ExportWorkadayContract;
use Illuminate\Http\Response;

class ExportWorkadayController extends Controller
{
    public function __construct(protected readonly ExportWorkadayContract $workadayContract)
    {
    }

    public function export(ExportWorkadayRequest $request): Response
    {
        return response($this->workadayContract->exportWorkaday($request->validated()), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="relatorio_workadays.pdf"'
        ]);
    }
}
