<?php

namespace App\Providers\Auth;

use App\Services\Auth\Contract\LoginContract;
use App\Services\Auth\LoginService;
use Illuminate\Support\ServiceProvider;

class LoginServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(LoginContract::class, function ($app) {
            return new LoginService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    public function provides(): array
    {
        return [LoginContract::class];
    }
}
