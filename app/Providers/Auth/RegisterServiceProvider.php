<?php

namespace App\Providers\Auth;

use App\Services\Auth\Contract\RegisterContract;
use App\Services\Auth\RegisterService;
use Illuminate\Support\ServiceProvider;

class RegisterServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(RegisterContract::class, function ($app) {
            return new RegisterService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    public function provides(): array
    {
        return [RegisterContract::class];
    }
}
