<?php

namespace App\Providers\Workaday;

use App\Services\Workaday\Contract\WorkadayContract;
use App\Services\Workaday\WorkadayService;
use Illuminate\Support\ServiceProvider;

class WorkadayServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(WorkadayContract::class, function ($app) {
            return new WorkadayService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    public function provides(): array
    {
        return [WorkadayContract::class];
    }
}
