<?php

namespace App\Providers\Workaday;

use App\Services\Workaday\Contract\ExportWorkadayContract;
use App\Services\Workaday\ExportWorkadayService;
use Illuminate\Support\ServiceProvider;

class ExportWorkadayServiceProvider extends ServiceProvider
{
    protected bool $defer = true;

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(ExportWorkadayContract::class, function ($app) {
            return new ExportWorkadayService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    public function provides(): array
    {
        return [ExportWorkadayContract::class];
    }
}
