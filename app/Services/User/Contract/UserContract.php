<?php

namespace App\Services\User\Contract;

use Illuminate\Support\Collection;

interface UserContract
{
    public function getUsersByCompany(): Collection;
    public function getUsersName(): Collection;
}
