<?php

namespace App\Services\User;

use App\Models\User;
use App\Services\User\Contract\UserContract;
use Illuminate\Support\Collection;

class UserService implements UserContract
{
    public function getUsersByCompany(): Collection
    {
        return User::query()->get();
    }

    public function getUsersName(): Collection
    {
        return User::query()->select('first_name', 'last_name')->get()->map(function($contact) {
            return ['name' => $contact->first_name . ' ' . $contact->last_name];
        })->pluck('name');
    }
}
