<?php

namespace App\Services\Workaday\Contract;

use App\Models\Workaday;
use Illuminate\Support\Collection;

interface WorkadayContract
{
    public function create(array $payload): Workaday;
    public function getAllWorkaday(): Collection;
    public function update(Workaday $workaday, array $payload): bool;
    public function getWorkadayFiltered(array $filters): Collection;
}
