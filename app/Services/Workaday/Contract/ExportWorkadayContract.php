<?php

namespace App\Services\Workaday\Contract;

interface ExportWorkadayContract
{
    public function exportWorkaday(array $filters): string;
}
