<?php

namespace App\Services\Workaday;

use App\Services\Workaday\Contract\ExportWorkadayContract;
use App\Services\Workaday\Contract\WorkadayContract;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;

class ExportWorkadayService implements ExportWorkadayContract
{
    public function exportWorkaday(array $filters): string
    {
        return $this->generatePdf(
            $this->generateHtml(app(WorkadayContract::class)->getWorkadayFiltered($filters))
        );
    }

    private function generateHtml($workadayList): string
    {
        $html = '<h1>Relatório de Workadays</h1>';

        foreach ($workadayList as $workaday) {
            $html .= '<h2>Nome: ' . $workaday->owner_name . '</h2>';

            $formattedDate = Carbon::parse($workaday->created_at)->locale('pt_BR')->isoFormat('dddd DD/MM/YYYY HH:mm');
            $html .= '<p>Data de criação: ' . $formattedDate . '</p>';
            $html .= $this->formatList('Alinhamentos', explode("\n", $workaday->calls));
            $html .= $this->formatList('Entregas', explode("\n", $workaday->done));
            $html .= $this->formatList('Contatos', json_decode($workaday->contacts, true));

            $html .= '<hr>';
        }

        return $html;
    }

    private function formatList(string $title, ?array $items): string
    {
        $html = '<p>' . $title . ': </p>';
        if (!empty($items)) {
            $html .= '<ul>';
            foreach ($items as $item) {
                $html .= '<li>' . $item . '</li>';
            }
            $html .= '</ul>';
        }

        return $html;
    }

    private function generatePdf(string $html): string
    {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf->setOptions($options);

        $dompdf->render();

        return $dompdf->output();
    }
}
