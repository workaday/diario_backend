<?php

namespace App\Services\Workaday;

use App\Models\Workaday;
use App\Services\Workaday\Contract\WorkadayContract;
use Illuminate\Support\Collection;

class WorkadayService implements WorkadayContract
{
    public function create(array $payload): Workaday
    {
        return Workaday::query()->create($this->formatPayload($payload));
    }

    public function getAllWorkaday(): Collection
    {
        return Workaday::query()->orderBy('created_at', 'desc')->get();
    }

    public function update(Workaday $workaday, array $payload): bool
    {
        return $workaday->update($this->formatPayload($payload));
    }

    private function formatPayload(array $payload): array
    {
        return [
            'calls' => $payload['calls'],
            'done' => $payload['done'],
            'contacts' => json_encode($payload['contacts']),
            'user_id' => $payload['user_id'],
            'owner_name' => $payload['user_name'],
        ];
    }

    public function getWorkadayFiltered(array $filters): Collection
    {
        return Workaday::query()
            ->select('owner_name', 'calls', 'done', 'contacts', 'created_at')
            ->when(isset($filters['start_date']) && $filters['start_date'], function ($query) use ($filters) {
                return $query->whereDate('created_at', '>=', $filters['start_date']);
            })
            ->when(isset($filters['end_date']) && $filters['end_date'], function ($query) use ($filters) {
                return $query->whereDate('created_at', '<=', $filters['end_date']);
            })
            ->when(isset($filters['user_id']) && $filters['user_id'], function ($query) use ($filters) {
                return $query->where('user_id', $filters['user_id']);
            })
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
