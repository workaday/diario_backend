<?php

namespace App\Services\Auth;

use App\Models\User;
use App\Services\Auth\Contract\LoginContract;
use Illuminate\Support\Facades\Auth;

class LoginService implements LoginContract
{
    public function makeLogin(string $userEmail): array
    {
        $user = Auth::user();

        return [
            'token' => $this->createBearerToken($user),
            'user_id' => $user->id,
            'user_name' => $user->name,
            'user_email' => $userEmail,
        ];
    }

    private function createBearerToken(User $user): string
    {
        return $user->createToken('API Token')->plainTextToken;
    }
}
