<?php

namespace App\Services\Auth;

use App\Models\User;
use App\Services\Auth\Contract\RegisterContract;
use Illuminate\Support\Facades\Hash;

class RegisterService implements RegisterContract
{
    public function makeRegister(array $userData): User
    {
        return User::query()->create([
            'first_name' => $userData['first_name'],
            'last_name' => $userData['last_name'],
            'job_description' => $userData['job_description'],
            'email' => $userData['email'],
            'password' => Hash::make($userData['password']),
        ]);
    }
}
