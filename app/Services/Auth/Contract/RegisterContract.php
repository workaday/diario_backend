<?php

namespace App\Services\Auth\Contract;

use App\Models\User;

interface RegisterContract
{
    public function makeRegister(array $userData): User;
}
