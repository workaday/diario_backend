<?php

namespace App\Services\Auth\Contract;

interface LoginContract
{
    public function makeLogin(string $userEmail): array;
}
