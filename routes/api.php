<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Workaday\ExportWorkadayController;
use App\Http\Controllers\Workaday\WorkadayController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('/users', [UserController::class, 'get']);
        Route::get('/contacts', [UserController::class, 'getAllUserNames']);

        Route::post('/workaday/create', [WorkadayController::class, 'post']);
        Route::get('/workadays', [WorkadayController::class, 'get']);
        Route::get('/workaday/{workaday}', [WorkadayController::class, 'show']);
        Route::delete('/workaday/{workaday}', [WorkadayController::class, 'delete']);
        Route::put('/workaday/{workaday}', [WorkadayController::class, 'put']);

        Route::post('/workaday/export', [ExportWorkadayController::class, 'export']);
    });
});
